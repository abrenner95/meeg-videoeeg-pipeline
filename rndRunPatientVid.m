% For the chosen patient, frequency band, channel choice (up, down, all) and
% parameters - the full table of all correlations is computed. This table is
% then cleaned based on the percentage of the data left after thresholding
% of singular channels and channel combination.
% The channels detected automatically are also removed

function [pairedChannelsNew,tbFull,meanCor,bestLag]=rndRunPatientVid(patNr, ~,lagStored,coefficients)

%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});


%% prepare parameters

p = coefficients(1); % p-th percentile
amTreshWin = coefficients(2);
winLength=coefficients(3);
amTresh = coefficients(4);

dataLoc = [folderLoc '/mEEG-' patNr ];

fpath = strcat(pwd(), '/storage/filtered/', patNr, '.mat');
load(fpath, 'MobVid');


% Get chosen channels
pairedChannels = cellstr(pairedChannelsImport([dataLoc '/pairedChannels_' 'vid' '.txt'],2,'LastRow')');

% Get all channels
channelList1 = MobVid.channelList2;

% Store chosen channels in numerical values for faster indexing the
% channels
pairedChannelsNum = zeros(size(pairedChannels));
for n = 1:size(pairedChannels, 2)
    pairedChannelsNum(1,n) = find(ismember(channelList1,pairedChannels(1,n))>0);
end

artifactIndices = zeros(size(pairedChannels));


load('rndnoise.mat');

%%

% Normalize EEG by threshold
t = MobVid.time;
[fullSignal1, rndnoise] = normalizeEEG(MobVid.signal2, rndnoise, p);


% Apply amplitude thresholding on all channels
for x = 1:size(pairedChannelsNum,2)
    % Threshold on EEG1 channel
    [index1, s1Thresholded] = thresholdEEG(fullSignal1(pairedChannelsNum(1,x),:), amTresh, amTreshWin, winLength);
    % Update stored signal data, so that we will not need to recompute
    % thresholding in further steps
    fullSignal1(pairedChannelsNum(1,x), :) = s1Thresholded;
    % Store how much data was left in percent in each channel
    artifactIndices(1,x) = index1;
end

[rndIndex, rndnoise] = thresholdEEG(rndnoise, amTresh, amTreshWin, winLength);

% Find all channels with more than 75% data left. Remove all channel 
% combinations where this is not the case
pos = abs(artifactIndices(1,:)) >= 0.7;

pairedChannels = pairedChannels(1,pos);
pairedChannelsNum = pairedChannelsNum(1,pos);


% Pre-define table entries (empty tables, if an all channel combinations too much data was removed)
FirstChM = {};
allCorr = []; Lags = []; DataLeft = []; GarbageInChannel = zeros(1,0); bestLag = [];

if (size(pairedChannels,2) > 1)

% Run all channel combinations and see which lag gives the best correlation
% allLags = zeros(1,nchoosek(size(pairedChannelsNum,2),2));
allLags = [];
n = 1;
for x = 1:size(pairedChannelsNum,2)
    bipolar1 = fullSignal1(pairedChannelsNum(1,x),:);
    bipolar2 = rndnoise(1,1:length(bipolar1)); %fliplr
    
    % l=0, computes lag automatically
    lag = findLag(lagStored, patNr, bipolar1, bipolar2);
    allLags(n) = lag;
    GarbageInChannel(:,n) = [artifactIndices(1,x)];
    n = n + 1;
end


% Set most frequent lag to new lag
bestLag = mode(allLags);

%bestLag
n=1;
for x= 1:size(pairedChannelsNum,2)
    if abs(allLags(n) - bestLag)<5
        lag = allLags(n);
    else
        lag = bestLag;
    end
    bipolar1 = fullSignal1(pairedChannelsNum(1,x),:);
    bipolar2 = rndnoise(1,1:length(bipolar1));
    [index, bipolar1, bipolar2] = shiftData(lag, bipolar1, bipolar2);
    
    channelCorr = corr(bipolar1', bipolar2','Type','Pearson');
    
    FirstChM(n) = pairedChannels(1,x); 
        allCorr(n) = channelCorr;
        Lags(n) = lag;
        DataLeft(n) = index; %size(toCompare1,2)/size(d1,2);
        n = n+1;
end

end

%
%Put results in a table

tbFull=table(FirstChM',allCorr',Lags', DataLeft',GarbageInChannel(1,:)','VariableNames',{'Ch1M' 'Correlation' 'Delay' 'Percent' 'PercentInChannel1'});

pairedChannelsNew = pairedChannels;

%Compute mean correlation
meanCor = nanmean(tbFull.Correlation);


end