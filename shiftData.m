function [dataLeft, bipolar1shifted, bipolar2shifted] = shiftData(lag, bipolar1, bipolar2)
%SHIFTDATA Summary of this function goes here
%   Detailed explanation goes here

%mEEG and vEEG are in different sizes now, find min
minSize = min(size(bipolar1,2),size(bipolar2,2));


bipolar1shifted = bipolar1(1,1:minSize);


% Shift the vEEG to match mEEG
bipolar2shifted = circshift(bipolar2', -lag)';
% Prune to smaller length
bipolar2shifted = bipolar2shifted(1,1:minSize);

% Find NaNs in both bipolar channels
isNotOK = ~(isfinite(bipolar1shifted) & isfinite(bipolar2shifted));
% Store how much data is left in each bipolar channel (relative to the pruned length)
dataLeft = sum(~isNotOK)/length(isNotOK);

% Prune NaNs in both bipolar channels
bipolar1shifted(isNotOK)=[];
bipolar2shifted(isNotOK)=[];

end

