% 2nd file to be executed.
% Makes "all" file out of 'up' and 'down' channels
% Automatically rejected channels are not considered


%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});


%% Loop over all patients
for ind = 1:21
    
    % Translate patient's number to string e.g. 1---> '01'
    if ind<10
        PatientNr = ['0' num2str(ind)];
    else
        PatientNr = num2str(ind);
    end
    
    % Read 'up' and 'down' for the given patient
    dataLoc = [folderLoc '/mEEG-' PatientNr];
    pairedChannelsAll = cellstr(pairedChannelsImport([dataLoc '/pairedChannels_' 'vTv_old' '.txt'],1,'LastRow'));
    
    % Load rejected channels
    rejChVideo = struct2cell(load([dataLoc '/AutomaticallyRejectedChannelsVideo' PatientNr], 'rejCh'));
    rejChVideo = rejChVideo{1};
    
    % Check whether rejected channels occur in channel combinations
    idxV = true(size(pairedChannelsAll,1),1);
    for n = 1:length(rejChVideo)
        idxV = idxV & ~strcmp(pairedChannelsAll(:,2), rejChVideo{1,n});
    end
    idx = find(idxV);
    
    % Only load channels that were not rejected
    pairedChannelsNew = pairedChannelsAll(idx,:);
    
    A = cell2table(pairedChannelsNew);
    B = cell2table(pairedChannelsAll);
    
    % Save
    writetable(A,[dataLoc '/pairedChannels_vTv'],'WriteVariableNames',false)
end