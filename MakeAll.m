% 2nd file to be executed.
% Makes "all" file out of 'up' and 'down' channels
% Automatically rejected channels are not considered


%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});


%% Loop over all patients
for ind = 1:21
    
    % Translate patient's number to string e.g. 1---> '01'
    if ind<10
        PatientNr = ['0' num2str(ind)];
    else
        PatientNr = num2str(ind);
    end
    
    % Read 'up' and 'down' for the given patient
    dataLoc = [folderLoc '/mEEG-' PatientNr];
    pairedChannelsUp = cellstr(pairedChannelsImport([dataLoc '/pairedChannels_' 'up' '.txt'],1,'LastRow'));
    pairedChannelsDown = cellstr(pairedChannelsImport([dataLoc '/pairedChannels_' 'down' '.txt'],2,'LastRow'));
    
    % Load rejected channels
    rejChMobile = struct2cell(load([dataLoc '/AutomaticallyRejectedChannelsMobile' PatientNr], 'rejCh'));
    rejChMobile = rejChMobile{1};
    rejChVideo = struct2cell(load([dataLoc '/AutomaticallyRejectedChannelsVideo' PatientNr], 'rejCh'));
    rejChVideo = rejChVideo{1};
    
    % Put channels together
    pairedChannelsAll = [pairedChannelsUp; pairedChannelsDown];
    
    % Check whether rejected channels occur in channel combinations
    idxV = true(size(pairedChannelsAll,1),1);
    for n = 1:length(rejChVideo)
        idxV = idxV & ~strcmp(pairedChannelsAll(:,2), rejChVideo{1,n});
    end
    idxM = true(size(pairedChannelsAll,1),1);
    for n = 1:length(rejChMobile)
        idxM = idxM & ~strcmp(pairedChannelsAll(:,1), rejChMobile{1,n});
    end
    idx = find(idxV & idxM);
    
    % Only load channels that were not rejected
    pairedChannelsNew = pairedChannelsAll(idx,:);
    
    A = cell2table(pairedChannelsNew);
    B = cell2table(pairedChannelsAll);
    
    % Save
    writetable(A,[dataLoc '/pairedChannels_all'],'WriteVariableNames',false)
    writetable(B,[dataLoc '/pairedChannels_all_old'],'WriteVariableNames',false)
end
