%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});

%% Parameters
%Here we can choose different cleaning options
noiseOptions = 0.7; % < 0.7 already removed
%Here we can take the parameters for which FullTables are pre-computed
p = 85;
amThresh = 8;
amThreshWin = 1.5;
winLength = 100;
freqBand = 'FullBand';
version = 'FullBand';
load('placement.mat');

%trainSet = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
%testSet = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
all = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
load('trainSet');
load('testSet');

n = 1;
mat = [];
grp = cell(0);
cgrp = cell(0);
corrCell = cell(1,length(all));
for ind = all % patient nr
    if ind<10
        patNr=['0' num2str(ind)];
    else
        patNr=num2str(ind);
    end
    if n<10
        newPatNr=['0' num2str(n)];
    else
        newPatNr=num2str(n);
    end
    % Load table
    load([folderLoc '\Tables\FullTable' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.mat'] ,'tbFull');
    
    
    % Select long pairs only
    tbLength = size(tbFull,1);
    distances = [];
    for counter = 1:tbLength
        % Compute Manhatten distance for both neighboring vEEG electrodes
        % to the reference electrode
        distances(counter,1) = makeManhatten(placement, tbFull.Ch1V(counter), tbFull.Ch2V(counter));
    end
    long_pairs = min(distances,[],2) >= 4;
    tbFull = tbFull(long_pairs,:); 
    
    
    mat = [mat; tbFull.Correlation];
    grp = [grp; repmat({newPatNr},size(tbFull.Correlation,1),1)];
    if ismember(ind, testSet)
        cgrp = [cgrp; 'r'];
    else
        cgrp = [cgrp; 'b'];
    end
    corrCell{n} = tbFull.Correlation;
    n = n + 1;
end

%figure
boxplot(mat, grp, 'ColorGroup', cgrp, 'Colors', 'br', 'Symbol', 'o')
title('Correlation per patient - mEEG to vEEG')
xlabel('Patient number')
ylabel('Correlation coefficient')
ylim([-1 1])
legend(findobj(gca,'Tag','Box'),'Training set','Test set','Location','southeast')
set(gca,'FontSize',12)

set(gcf,'color','white')

fig_print(gcf, 'boxplot.png',[1000, 800],1)