%%
% Clean data by running windowed and amplitude thresholding, put NaN in
% the gaps to preserve the vector length. Compute "index": how much data is
% left after the cleaning.

function [index,dataClean] = thresholdEEG(data, amTresh, amTreshWin, winLength)

% Thresholding by windowing
dataClean = data;

% Here we take a non-overlapping window and compute
% abs(amplitude) average inside it.
averages = zeros(1, floor(size(dataClean,2) / winLength));
winPosition= 1;
for n = 1:size(averages,2)
   averages(n) = mean(abs(dataClean(1,winPosition:winPosition+winLength-1))) ;
   winPosition = winPosition + winLength;
end
averagesStretched = zeros(1,size(dataClean,2));
for n = 1:size(averages,2)
    averagesStretched(n*winLength-(winLength-1):n*winLength) = averages(n)*ones(1,winLength);
end
% Find and reject all intervals where the average is above the threshold
indices = find(abs(averagesStretched) > amTreshWin);
dataClean(:, indices')= NaN;

% Thresholding usual amplitude
indices = find(abs(dataClean) > amTresh);
dataClean(:, indices') = NaN;
    
% Compute the percentage of data left after the cleaning
index = sum(isfinite(dataClean)/size(dataClean,2));

end
