%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});

%% Parameters
load('placement.mat');
g2 = 'G2';
%Here we can choose different cleaning options
noiseOptions = 0.7; % < 0.7 already removed
%Here we can take the parameters for which FullTables are pre-computed
pList = 85; % 85
amThreshList = 8; % 6
amThreshWinList = 1.5; % 1.5
winLengthList = 100; % 100
freqBand = 'vTv';
version = 'vTv';

trainSet = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
testSet = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
%load('trainSet');
%load('testSet');


%% Compute channels left
% Load original number of channels
numCombOld = zeros(1,length(trainSet));
n = 1;
for ind = trainSet
    if ind<10
        patNr=['0' num2str(ind)];
    else
        patNr=num2str(ind);
    end
    % Load all original channel combinations
    pairedChannels = cellstr(pairedChannelsImport([folderLoc '/mEEG-' patNr '/pairedChannels_' version '_old.txt'],2,'LastRow')');
    distances = [];
    counter = 1;
    for x = 1:size(pairedChannels,2)
        distances(counter,1) = makeManhatten(placement, g2, pairedChannels(1,x));
        distances(counter,2) = makeManhatten(placement, g2, pairedChannels(2,x));
        counter = counter + 1;
    end
    long_pairs = min(distances,[],2) >= 3;
    numCombOld(n) = sum(long_pairs);
    n = n + 1;
end


%% Iterate over all parameters
vChannelsLeft = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList));
vPercent = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList));
vDataLeft = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList));
vCorr = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
vP = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
vAmThresh = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
vAmThreshWin = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
vWinLength = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
n = 1;
for p = pList
    for amThresh = amThreshList
        for amThreshWin = amThreshWinList
            for winLength = winLengthList
                channelsLeftAll = zeros(1,length(trainSet));
                percentAll = zeros(1,length(trainSet));
                dataLeftAll = zeros(1,length(trainSet));
                corrAll = [];
                indC = 1;
                for ind = trainSet % patient nr
                    if ind<10
                        patNr=['0' num2str(ind)];
                    else
                        patNr=num2str(ind);
                    end
                    % Load table
                    load([folderLoc '\Tables\FullTable' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.mat'] ,'tbFull');
                    % Load channel combination
                    pairedChannels = readtable([folderLoc '/mEEG-' patNr '/PairedChannels' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.txt'], 'ReadVariableNames', false);
                    % Select long pairs only
                    tbLength = size(tbFull,1);
                    distances = [];
                    for counter = 1:tbLength
                        % Compute Manhatten distance for both neighboring vEEG electrodes
                        % to the reference electrode
                        distances(counter,1) = makeManhatten(placement, g2, tbFull.Ch1M(counter));
                        distances(counter,2) = makeManhatten(placement, g2, tbFull.Ch1V(counter));
                        counter = counter + 1;
                    end
                    long_pairs = min(distances,[],2) >= 3;
                    tbFull = tbFull(long_pairs,:); 
                    
                    numCombNew = sum(long_pairs);
                    channelsLeft = numCombNew / numCombOld(indC);
                    channelsLeftAll(indC) = channelsLeft;
                    
                    percent = nanmean(tbFull.Percent);
                    percentAll(indC) = percent;
                    
                    dataLeftAll(indC) = channelsLeft * percent;
                    
                    corr = zeros(size(tbFull.Correlation));
                    for corrC = 1:length(tbFull.Correlation)
                        r = tbFull.Correlation(corrC);
                        corr(corrC) = atanh(r);
                    end
                    corrAll = [corrAll; corr];
                    
                    indC = indC + 1;
                end
                
                vChannelsLeft(n) = nanmean(channelsLeftAll);
                vPercent(n) = nanmean(percentAll);
                vDataLeft(n) = nanmean(dataLeftAll);
                corrTrans = nanmean(corrAll);
                corrRev = tanh(corrTrans);
                vCorr(n) = corrRev;
                vP(n) = p;
                vAmThresh(n) = amThresh;
                vAmThreshWin(n) = amThreshWin;
                vWinLength(n) = winLength;
                
                n = n + 1;
            end
        end
    end
end

tb = table(vP',vAmThresh', vAmThreshWin',vWinLength',vCorr',vChannelsLeft',vPercent',vDataLeft','VariableNames',...
    {'Percentile','AmThresh', 'AmThreshWin','WinLength','MeanCorr','ChannelsLeft','DataLeftPerChannel','DataLeftTotal'});

% Remove parameter combinations where less than 70 percent of data in total
% is left
tb2 = tb(tb.DataLeftTotal>=0.0,:);

tb3 = tb2(tb2.MeanCorr==max(tb2.MeanCorr),:);

p = tb3.Percentile;
amThresh = tb3.AmThresh;
amThreshWin = tb3.AmThreshWin;
winLength = tb3.WinLength;

numCombOld = zeros(1,length(testSet));
n = 1;
for ind = testSet
    if ind<10
        patNr=['0' num2str(ind)];
    else
        patNr=num2str(ind);
    end
    % Load all original channel combinations
    pairedChannels = cellstr(pairedChannelsImport([folderLoc '/mEEG-' patNr '/pairedChannels_' version '_old.txt'],2,'LastRow')');
    distances = [];
    counter = 1;
    for x = 1:size(pairedChannels,2)
        distances(counter,1) = makeManhatten(placement, g2, pairedChannels(1,x));
        distances(counter,2) = makeManhatten(placement, g2, pairedChannels(2,x));
        counter = counter + 1;
    end
    long_pairs = min(distances,[],2) >= 3;
    numCombOld(n) = sum(long_pairs);
    n = n + 1;
end

channelsLeftAll = zeros(1,length(testSet));
percentAll = zeros(1,length(testSet));
dataLeftAll = zeros(1,length(testSet));
corrAll = [];
indC = 1;
for ind = testSet % patient nr
    if ind<10
        patNr=['0' num2str(ind)];
    else
        patNr=num2str(ind);
    end
    % Load table
    load([folderLoc '\Tables\FullTable' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.mat'] ,'tbFull');
    % Load channel combination
    pairedChannels = readtable([folderLoc '/mEEG-' patNr '/PairedChannels' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.txt'], 'ReadVariableNames', false);
    % Select long pairs only
    tbLength = size(tbFull,1);
    distances = [];
    for counter = 1:tbLength
        % Compute Manhatten distance for both neighboring vEEG electrodes
        % to the reference electrode
        distances(counter,1) = makeManhatten(placement, g2, tbFull.Ch1M(counter));
        distances(counter,2) = makeManhatten(placement, g2, tbFull.Ch1V(counter));
        counter = counter + 1;
    end
    long_pairs = min(distances,[],2) >= 3;
    tbFull = tbFull(long_pairs,:); 
    
    numCombNew = sum(long_pairs);
    channelsLeft = numCombNew / numCombOld(indC);
    channelsLeftAll(indC) = channelsLeft;
    
    percent = nanmean(tbFull.Percent);
    percentAll(indC) = percent;
    
    dataLeftAll(indC) = channelsLeft * percent;
    
    corr = zeros(size(tbFull.Correlation));
    for corrC = 1:length(tbFull.Correlation)
        r = tbFull.Correlation(corrC);
        corr(corrC) = atanh(r);
    end
    corrAll = [corrAll; corr];
    
    indC = indC + 1;
end

vChannelsLeft = nanmean(channelsLeftAll);
vPercent = nanmean(percentAll);
vDataLeft = nanmean(dataLeftAll);
corrTrans = nanmean(corrAll);
corrRev = tanh(corrTrans);
vCorr = corrRev;
vP = p;
vAmThresh = amThresh;
vAmThreshWin = amThreshWin;
vWinLength = winLength;

tbt = table(vP',vAmThresh', vAmThreshWin',vWinLength',vCorr',vChannelsLeft',vPercent',vDataLeft','VariableNames',...
    {'Percentile','AmThresh', 'AmThreshWin','WinLength','MeanCorr','ChannelsLeft','DataLeftPerChannel','DataLeftTotal'});