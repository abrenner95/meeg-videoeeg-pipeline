% Load all original channel combinations
pairedChannelsAll = cellstr(pairedChannelsImport(['C:\Users\abrenner\Desktop', '/new.txt'],1,'LastRow'));

pairedChannelsNew = cell(0,2);

for n = 1:length(pairedChannelsAll)
    
    isUnique = true;
    for k = 1:size(pairedChannelsNew,1)
        if (strcmp(pairedChannelsAll(n,1),pairedChannelsNew(k,1)) || strcmp(pairedChannelsAll(n,2),pairedChannelsNew(k,1)) || strcmp(pairedChannelsAll(n,1),pairedChannelsNew(k,2)) || strcmp(pairedChannelsAll(n,2),pairedChannelsNew(k,2)))
            isUnique = false;
        end
    end
    
    if isUnique
       pairedChannelsNew = [pairedChannelsNew; pairedChannelsAll(n,:)];
    end
    
end