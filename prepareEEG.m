% 3rd file to be executed.
% Pre-filtering the EEG data

%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});


%% Parameters to set:
bandNr = 1; % 1


%% Get data in the chosen band
bands = [1 38; 1 3.5; 3.5 7.5; 7.5 12.5; 12.5 30]; % Frequency bands of interest
fq_highpass = bands(bandNr, 1);
fq_lowpass = bands(bandNr, 2);


%% Loop over patients
for ind = 1:21 % Here go patient's numbers
    
    % Translates patient's number to string e.g. 1---> '01'
    if ind<10
        patNr = ['0' num2str(ind)];
    else
        patNr = num2str(ind);
    end
    
    % Where is the EEG data?
    dataLoc = [folderLoc '/mEEG-' patNr];
    
    EEG1 = pop_biosig(strcat(dataLoc, '/mEEG-', patNr, '#mobile.edf'));
    EEG2 = pop_biosig(strcat(dataLoc, '/mEEG-', patNr, '#video.edf'),'channels',1:21);
    EEG1 = eeg_checkset(EEG1);
    EEG2 = eeg_checkset(EEG2);
    
    MobVid = EEGdataHandler(EEG1, EEG2);
    
    [d1, d2, t] = filterEEG(EEG1, EEG2, fq_highpass, fq_lowpass);
    
    MobVid.signal1 = d1;
    MobVid.signal2 = d2;
    MobVid.time = t;
    
    % Store all relevant data into .mat files
    fpath = strcat(pwd(), '/storage/filtered/', patNr, '.mat');
    save(fpath, 'MobVid');
end