function dist = makeManhatten(placement, electrode_name1, electrode_name2)
    if strcmp(electrode_name1, 'G2')
        idxC1 = [2.5, 4.5];
    else
        [idx1, idx2] = find(strcmp(placement, electrode_name1));
        idxC1 = [idx1, idx2];
    end


    [idx1, idx2] = find(strcmp(placement, electrode_name2));
    idxC2 = [idx1, idx2];

    dist = sum(abs(idxC1-idxC2));
end

