% 2nd file to be executed.
% Makes "all" file out of 'up' and 'down' channels
% Automatically rejected channels are not considered


%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});


%% Loop over all patients
for ind = 1:21
    
    % Translate patient's number to string e.g. 1---> '01'
    if ind<10
        PatientNr = ['0' num2str(ind)];
    else
        PatientNr = num2str(ind);
    end
    
    % Read 'up' and 'down' for the given patient
    dataLoc = [folderLoc '/mEEG-' PatientNr];
    pairedChannelsRevMob = cellstr(pairedChannelsImport([dataLoc '/pairedChannels_' 'mob_old' '.txt'],1,'LastRow'));
    pairedChannelsRevVid = cellstr(pairedChannelsImport([dataLoc '/pairedChannels_' 'vid_old' '.txt'],1,'LastRow'));
    
    % Load rejected channels
    rejChMobile = struct2cell(load([dataLoc '/AutomaticallyRejectedChannelsMobile' PatientNr], 'rejCh'));
    rejChMobile = rejChMobile{1};
    rejChVideo = struct2cell(load([dataLoc '/AutomaticallyRejectedChannelsVideo' PatientNr], 'rejCh'));
    rejChVideo = rejChVideo{1};
    
    % Check whether rejected channels occur in channel combinations
    idxV = true(size(pairedChannelsRevVid,1),1);
    for n = 1:length(rejChVideo)
        idxV = idxV & ~strcmp(pairedChannelsRevVid(:,1), rejChVideo{1,n});
    end
    pairedChannelsRevVid = pairedChannelsRevVid(idxV,:);
    
    idxM = true(size(pairedChannelsRevMob,1),1);
    for n = 1:length(rejChMobile)
        idxM = idxM & ~strcmp(pairedChannelsRevMob(:,1), rejChMobile{1,n});
    end
    pairedChannelsRevMob = pairedChannelsRevMob(idxM,:);
    
    A = cell2table(pairedChannelsRevMob);
    B = cell2table(pairedChannelsRevVid);
    
    % Save
    writetable(A,[dataLoc '/pairedChannels_mob'],'WriteVariableNames',false)
    writetable(B,[dataLoc '/pairedChannels_vid'],'WriteVariableNames',false)
end
