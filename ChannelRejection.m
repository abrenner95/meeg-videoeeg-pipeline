% 1st file to be executed.
% For a given patient automatic channel rejection from EEGLAB is used
% Names of the rejected channels are stored


%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});


%% Iteration over each patient
for ind = 1:21 % Here go patient's numbers
    
    % Translates patient's number to string e.g. 1---> '01'
    if ind<10
        patNr = ['0' num2str(ind)];
    else
        patNr = num2str(ind);
    end
    
    
    % Where is the EEG data?
    dataLoc = [folderLoc '/mEEG-' patNr ];
    
    for type1=1:2 % 1==mobile, 2==video
        % Get data
        if type1==1
            EEG = pop_biosig([dataLoc '/mEEG-' patNr '#mobile.edf']);
            ext = 'Mobile';
            electrodes = 1:14;
        else
            EEG = pop_biosig([dataLoc '/mEEG-' patNr '#video.edf']);
            ext = 'Video';
            electrodes = 1:21;
        end
        
        
        % Use EEGLAB rejection function
        [~, indelec, measure, com]  = pop_rejchan(EEG, 'elec',electrodes ,'threshold',5,'norm','on','measure','kurt');
        
        
        % Save the rejected channels (separately for video and mobile
        aa = struct2cell(EEG.chanlocs);
        rejCh = aa(1,indelec);
        save([dataLoc '/AutomaticallyRejectedChannels' ext patNr ] , 'rejCh')
        
    end
end