%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});

%% Parameters
%Here we can choose different cleaning options
noiseOptions = 0.7; % < 0.7 already removed
%Here we can take the parameters for which FullTables are pre-computed
p = 85;
amThresh = 6;
amThreshWin = 1.5;
winLength = 100;
freqBand = 'rnd_mob'; % rev_vid

%trainSet = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
%testSet = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
all = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
load('trainSet');
load('testSet');

n = 1;
mat = [];
grp = cell(0);
cgrp = cell(0);
corrCell = cell(1,length(all));
for ind = all % patient nr
    if ind<10
        patNr=['0' num2str(ind)];
    else
        patNr=num2str(ind);
    end
    if n<10
        newPatNr=['0' num2str(n)];
    else
        newPatNr=num2str(n);
    end
    % Load table
    load([folderLoc '\Tables\FullTable' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.mat'] ,'tbFull');
    
    mat = [mat; tbFull.Correlation];
    grp = [grp; repmat({newPatNr},size(tbFull.Correlation,1),1)];
    if ismember(ind, testSet)
        cgrp = [cgrp; 'r'];
    else
        cgrp = [cgrp; 'b'];
    end
    corrCell{n} = tbFull.Correlation;
    n = n + 1;
end

%figure
boxplot(mat, grp, 'ColorGroup', cgrp, 'Colors', 'br', 'Symbol', 'o')
title('Correleation per patient - Mobile records correlated with random noise')
xlabel('Patient number')
ylabel('Correlation')
legend(findobj(gca,'Tag','Box'),'Training set','Test set')
set(gca,'FontSize',12)
set(gcf,'color','white')
ax = gca;
ax.YTickLabel=cellstr(num2str(ax.YTick'));

fig_print(gcf, 'boxplot.png',[1000, 800],1)
%fig_print(gcf, 'boxplot.png',[750, 800],1)


meanPerP = zeros(1, length(corrCell));
for n = 1:length(corrCell)
    meanPerP(n) = nanmean(corrCell{n});
end
meanTotal = nanmean(meanPerP);
fprintf('Mean Correlation: %f', meanTotal);