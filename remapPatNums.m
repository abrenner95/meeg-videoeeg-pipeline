%% Values to be set
oldSet = [20    15    10    11    14];



%% Algorithm
original = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
new = 1:19;

mapping = [original; new];


newSet = zeros(1,length(oldSet));

for n = 1:length(oldSet)
    pos = find(original==oldSet(n));
    newSet(n) = pos;
end


strjoin(arrayfun(@(x) num2str(x),oldSet,'UniformOutput',false),',')

strjoin(arrayfun(@(x) num2str(x),newSet,'UniformOutput',false),', ')