% Takes 2 opened .edf files (typically 1 mobile and 1 video)
% Stores signal data, sampling rate, times and channelList for each EEG

classdef EEGdataHandler
    
    
    properties (Access = public)
        filename1;
        filename2;
        signal1;
        signal2;
        time;
        samplingRate1;
        samplingRate2;
        channelList1;
        channelList2;
        
    end
    
    methods (Access=public)
        
        function obj = EEGdataHandler(EEG1, EEG2)
            
            obj.filename1 = EEG1.filename; % Works only with modified EEGLAB
            obj.filename2 = EEG2.filename; % Works only with modified EEGLAB
            obj.signal1 = double(EEG1.data);
            obj.signal2 = double(EEG2.data);
            obj.time = EEG1.times;
            obj.samplingRate1 = EEG1.srate;
            obj.samplingRate2 = EEG2.srate;
            channelList1 = struct2cell(EEG1.chanlocs);
            channelList2 = struct2cell(EEG2.chanlocs);
            obj.channelList1 = channelList1(1,:);
            obj.channelList2 = channelList2(1,:);
        end
        
    end
end