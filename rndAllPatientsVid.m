% Run bunch of patients for different parameter, save the tables for the
% further optimization


%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});

dataLoc = strcat(folderLoc, '/Tables');


%% Data to be set:
indices = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
lagStored = 0; % 1 = pre-computed, 0 = compute
storeLag = false;
% Parameters
pList = [85]; % [80:2.5:97.5, 99]
amThreshList = [6]; % [1.5] % 0.5:0.5:2.5
amThreshWinList = [1.5]; % [1] % 0.2:0.2:0.8
winLengthList = [100]; % [10 20 100]
freqBand = 'rnd_vid';

tic

allCorr = zeros(1,length(indices));
n = 1;
for p = pList
    for amThresh = amThreshList
        for amThreshWin = amThreshWinList
            for winLength = winLengthList
                
                coefficients = [p, amThreshWin, winLength, amThresh];
                % Print current parameter settings to see progress during runtime
                fprintf('p: %d, amThresh: %.2f, amThreshWin: %.2f, winLength: %d\n', p, amThresh, amThreshWin, winLength);
                
                for ind = indices
            
                    if ind<10
                        patNr=['0' num2str(ind)];
                    else
                        patNr=num2str(ind);
                    end
                    
                    % Print patient number to see progress during runtime
                    fprintf(strcat('p', patNr, ';'));
                    
                    tPath = [dataLoc '/FullTable' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.mat'];
                    %if ~(exist(tPath,'file') == 2)
                        
                        [pairedChannelsNew,tbFull,meanCor,bestLag] = rndRunPatientVid(patNr, 0, lagStored, coefficients);
                        
                        % save lag
                        if storeLag
                            fpath = strcat(pwd(), '/storage/lags/', patNr, '.mat');
                            lag = bestLag;
                            save(fpath, 'lag');
                        end
                        
                        allCorr(1,n) = meanCor;
                        n = n + 1;
                        
                        A = cell2table(pairedChannelsNew);
                        % Save
                        cpath = [folderLoc '/mEEG-' patNr '/PairedChannels' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.txt'];
                        writetable(A, cpath,'WriteVariableNames',false,'FileType','text')
                        
                        save(tPath, 'tbFull')
                    %end 
                end
                fprintf('\n');
            end
        end
    end
end

toc