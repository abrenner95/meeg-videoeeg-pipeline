%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});

%% Parameters
%Here we can choose different cleaning options
noiseOption = 0.7; % < 0.7
%Here we can take the parameters for which FullTables are pre-computed
pList = [80, 85, 90, 95, 97, 99];  % 99
amThreshList = [1, 1.5, 2, 3, 4, 5, 6, 7]; % 4
amThreshWinList = [0.5, 1.0, 1.2, 1.5, 2, 2.5]; % 1
winLengthList = [15, 50, 75, 100, 200, 250]; % 50
freqBand = 'FullBand';
version = 'all';

%trainSet = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
%testSet = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];
load('trainSet');
load('testSet');

% Load original number of channels
numCombOld = zeros(1,length(trainSet));
n = 1;
for ind = trainSet
    if ind<10
        patNr=['0' num2str(ind)];
    else
        patNr=num2str(ind);
    end
    % Load all original channel combinations
    pairedChannels = cellstr(pairedChannelsImport([folderLoc '/mEEG-' patNr '/pairedChannels_' version '_old.txt'],2,'LastRow')');
    numCombOld(n) = nchoosek(size(pairedChannels,2),2);
    n = n + 1;
end

% Compute table entries for each patient for each parameter setting
n = 1;
vectorNumCombNew = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList)*length(trainSet));
vectorCorr = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList)*length(trainSet));
vectorPercent = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList)*length(trainSet));
vectorP = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList)*length(trainSet));
vectorAmThresh = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList)*length(trainSet));
vectorAmThreshWin = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList)*length(trainSet));
vectorWinLength = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList)*length(trainSet));
vectorInd = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList)*length(trainSet));
vectorNumCombOld = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList)*length(trainSet));
for p = pList
    for amThresh = amThreshList
        for amThreshWin = amThreshWinList
            for winLength = winLengthList
                c = 1;
                for ind = trainSet % patient nr
                    
                    if ind<10
                        patNr=['0' num2str(ind)];
                    else
                        patNr=num2str(ind);
                    end
                    % Load table
                    load([folderLoc '\Tables\FullTable' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.mat'] ,'tbFull');
                    % Load channel combination
                    pairedChannels = readtable([folderLoc '/mEEG-' patNr '/PairedChannels' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.txt'], 'ReadVariableNames', false);
                    if size(pairedChannels,2) > 1
                        vectorNumCombNew(n) = nchoosek(size(pairedChannels,2),2);
                    else
                        vectorNumCombNew(n) = 0;
                    end
                    
                    vectorCorr(n) = nanmean(tbFull.Correlation);
                    vectorPercent(n) = nanmean(tbFull.Percent);
                    vectorP(n) = p;
                    vectorAmThresh(n) = amThresh;
                    vectorAmThreshWin(n) = amThreshWin;
                    vectorWinLength(n) = winLength;
                    vectorInd(n) = ind;
                    vectorNumCombOld(n) = numCombOld(c);
                        
                    n = n+1;
                    c = c+1;
                end
            end
        end
    end
end

vectorChannelsLeft = vectorNumCombNew ./ vectorNumCombOld;
vectorDataLeft = vectorChannelsLeft .* vectorPercent;

tb = table(vectorP', vectorAmThresh',  vectorAmThreshWin', vectorWinLength', vectorInd',vectorCorr',vectorNumCombOld',vectorNumCombNew',vectorChannelsLeft',vectorPercent',vectorDataLeft','VariableNames',...
    {'Percentile','AmThresh', 'AmThreshWin','WinLength','PatientNumber','MeanCorr','OriginalSize','CleanSize','ChannelsLeft','DataLeftPerChannel','DataLeftTotal'});


% For each set of the parameters compute the average across patients (mean
% of means)
n = 1;
vectorCorr = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
vectorPercent = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
vectorP = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
vectorAmThresh = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
vectorAmThreshWin = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
vectorWinLength = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
vectorChannelsLeft = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
vectorDataLeft = zeros(1,length(pList)*length(amThreshList)*length(amThreshWinList)*length(winLengthList));
for p = pList
    for amThresh = amThreshList
        for amThreshWin = amThreshWinList
            for winLength = winLengthList
                tb1=tb(tb.Percentile==p,:);
                tb1=tb1(tb1.AmThresh==amThresh,:);
                tb1=tb1(tb1.AmThreshWin==amThreshWin,:);
                tb1=tb1(tb1.WinLength==winLength,:);
                
                vectorCorr(n) = nanmean(tb1.MeanCorr);
                vectorPercent(n) = nanmean(tb1.DataLeftPerChannel);
                vectorP(n) = p;
                vectorAmThresh(n) = amThresh;
                vectorAmThreshWin(n) = amThreshWin;
                vectorWinLength(n) = winLength;
                
                vectorChannelsLeft(n) = nanmean(tb1.ChannelsLeft);
                vectorDataLeft(n) = nanmean(tb1.DataLeftTotal);
                
                n = n + 1;
            end
        end
    end
end

tb2 = table(vectorP', vectorAmThresh',  vectorAmThreshWin', vectorWinLength',vectorCorr',vectorChannelsLeft',vectorPercent',vectorDataLeft','VariableNames',...
    {'Percentile','AmThresh', 'AmThreshWin','WinLength','MeanCorr','ChannelsLeft','DataLeftPerChannel','DataLeftTotal'});

% Remove parameter combinations where less than 70 percent of data in total
% is left
tb3 = tb2(tb2.DataLeftTotal>=0.7,:);

tb4 = tb3(tb3.MeanCorr==max(tb3.MeanCorr),:);


p = tb4.Percentile;
amThresh = tb4.AmThresh;
amThreshWin = tb4.AmThreshWin;
winLength = tb4.WinLength;

vectorNumCombNew = zeros(1,length(testSet));
vectorCorr = zeros(1,length(testSet));
vectorPercent = zeros(1,length(testSet));
vectorP = zeros(1,length(testSet));
vectorAmThresh = zeros(1,length(testSet));
vectorAmThreshWin = zeros(1,length(testSet));
vectorWinLength = zeros(1,length(testSet));
vectorInd = zeros(1,length(testSet));
vectorNumCombOld = zeros(1,length(testSet));
% TestSet
n = 1;
for ind = testSet
    if ind<10
        patNr=['0' num2str(ind)];
    else
        patNr=num2str(ind);
    end
    
    % Load all original channel combinations
    pairedChannels = cellstr(pairedChannelsImport([folderLoc '/mEEG-' patNr '/pairedChannels_' version '_old.txt'],2,'LastRow')');
    numCombOld(n) = nchoosek(size(pairedChannels,2),2);
   
    % Load table
    load([folderLoc '\Tables\FullTable' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.mat'] ,'tbFull');
    % Load channel combination
    pairedChannels = readtable([folderLoc '/mEEG-' patNr '/PairedChannels' patNr '_' num2str(p) '_' num2str(amThresh) '_' num2str(amThreshWin) '_' num2str(winLength) '_' freqBand '.txt'], 'ReadVariableNames', false);
    if size(pairedChannels,2) > 1
        vectorNumCombNew(n) = nchoosek(size(pairedChannels,2),2);
    else
        vectorNumCombNew(n) = 0;
    end
    
    vectorCorr(n) = nanmean(abs(tbFull.Correlation));
    vectorPercent(n) = nanmean(tbFull.Percent);
    vectorP(n) = p;
    vectorAmThresh(n) = amThresh;
    vectorAmThreshWin(n) = amThreshWin;
    vectorWinLength(n) = winLength;
    vectorInd(n) = ind;
    vectorNumCombOld(n) = numCombOld(n);
    
    n = n + 1;
end

vectorChannelsLeft = vectorNumCombNew ./ vectorNumCombOld;
vectorDataLeft = vectorChannelsLeft .* vectorPercent;

tbt1 = table(vectorP', vectorAmThresh',  vectorAmThreshWin', vectorWinLength', vectorInd',vectorCorr',vectorNumCombOld',vectorNumCombNew',vectorChannelsLeft',vectorPercent',vectorDataLeft','VariableNames',...
    {'Percentile','AmThresh', 'AmThreshWin','WinLength','PatientNumber','MeanCorr','OriginalSize','CleanSize','ChannelsLeft','DataLeftPerChannel','DataLeftTotal'});

meanCorr = nanmean(vectorCorr);
channelsLeft = nanmean(vectorChannelsLeft);
percent = nanmean(vectorPercent);
dataLeft = nanmean(vectorDataLeft);
tbt2 = table(p, amThresh,  amThreshWin, winLength,meanCorr,channelsLeft,percent,dataLeft,'VariableNames',...
    {'Percentile','AmThresh', 'AmThreshWin','WinLength','MeanCorr','ChannelsLeft','DataLeftPerChannel','DataLeftTotal'});

tb4
tbt2