% Very first data preparation
% Filtering

function [data1, data2, times]= filterEEG(EEGstruct1, EEGstruct2, fq_highpass, fq_lowpass)

% Low pass
ff1 = pop_eegfiltnew(EEGstruct1, [], fq_lowpass, 2000, 0, [], 0);
ff2 = pop_eegfiltnew(EEGstruct2, [], fq_lowpass, 2000, 0, [], 0);
ff1 = eeg_checkset(ff1);
ff2 = eeg_checkset(ff2);

% Resample
ff1 = pop_resample(ff1, min(EEGstruct1.srate, EEGstruct2.srate));
ff2 = pop_resample(ff2, min(EEGstruct1.srate, EEGstruct2.srate));
ff1 = eeg_checkset(ff1);
ff2 = eeg_checkset(ff2);

% High pass
ff1 = pop_eegfiltnew(ff1,fq_highpass,[], 2000, 0, [], 0);
ff2 = pop_eegfiltnew(ff2, fq_highpass, [], 2000, 0, [], 0);
ff1 = eeg_checkset(ff1);
ff2 = eeg_checkset(ff2);


% Single to double
ff1.data=double(ff1.data);
ff2.data=double(ff2.data);


% Get EEG data
data1 = ff1.data;
data2 = ff2.data;


%get time stamps from the first (typically mobile data)
times = double(ff1.times(:,1:min(size(ff1.data,2),size(ff2.data,2))));

end