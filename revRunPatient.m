% For the chosen patient, frequency band, channel choice (up, down, all) and
% parameters - the full table of all correlations is computed. This table is
% then cleaned based on the percentage of the data left after thresholding
% of singular channels and channel combination.
% The channels detected automatically are also removed

function [pairedChannelsNew,tbFull,meanCor,bestLag]=revRunPatient(patNr, ~,lagStored,coefficients)

%% Folder location
fileID = fopen(strcat(pwd(), '/folderLoc.txt'), 'r');
fileContent = textscan(fileID, '%s', 'delimiter','\n');
fclose(fileID);
folderLoc = char(fileContent{1});


%% prepare parameters

p = coefficients(1); % p-th percentile
amTreshWin = coefficients(2);
winLength=coefficients(3);
amTresh = coefficients(4);

dataLoc = [folderLoc '/mEEG-' patNr ];

fpath = strcat(pwd(), '/storage/filtered/', patNr, '.mat');
load(fpath, 'MobVid');


% Get chosen channels
pairedChannelsMob = cellstr(pairedChannelsImport([dataLoc '/pairedChannels_' 'mob' '.txt'],2,'LastRow')');
pairedChannelsMob = pairedChannelsMob(1,:);
pairedChannelsVid = cellstr(pairedChannelsImport([dataLoc '/pairedChannels_' 'vid' '.txt'],2,'LastRow')');
pairedChannelsVid = pairedChannelsVid(1,:);

% Get all channels
channelList1 = MobVid.channelList1;
channelList2 = MobVid.channelList2;

% Store chosen channels in numerical values for faster indexing the
% channels
pairedChannelsNumMob = zeros(size(pairedChannelsMob));
for n = 1:size(pairedChannelsMob, 2)
    pairedChannelsNumMob(1,n) = find(ismember(channelList1,pairedChannelsMob(1,n))>0);
end

artifactIndicesMob = zeros(size(pairedChannelsMob));

pairedChannelsNumVid = zeros(size(pairedChannelsVid));
for n = 1:size(pairedChannelsVid, 2)
    pairedChannelsNumVid(1,n) = find(ismember(channelList2,pairedChannelsVid(1,n))>0);
end

artifactIndicesVid = zeros(size(pairedChannelsVid));

%%

% Normalize EEG by threshold
t = MobVid.time;
[fullSignal1, fullSignal2] = normalizeEEG(MobVid.signal1, MobVid.signal2, p);


% Apply amplitude thresholding on all channels
for x = 1:size(pairedChannelsNumMob,2)
    % Threshold on EEG1 channel
    [index1, s1Thresholded] = thresholdEEG(fullSignal1(pairedChannelsNumMob(1,x),:), amTresh, amTreshWin, winLength);
    % Update stored signal data, so that we will not need to recompute
    % thresholding in further steps
    fullSignal1(pairedChannelsNumMob(1,x), :) = s1Thresholded;
    % Store how much data was left in percent in each channel
    artifactIndicesMob(1,x) = index1;
end
% Apply amplitude thresholding on all channels
for x = 1:size(pairedChannelsNumVid,2)
    % Threshold on EEG1 channel
    [index2, s2Thresholded] = thresholdEEG(fullSignal2(pairedChannelsNumVid(1,x),:), amTresh, amTreshWin, winLength);
    % Update stored signal data, so that we will not need to recompute
    % thresholding in further steps
    fullSignal2(pairedChannelsNumVid(1,x), :) = s2Thresholded;
    % Store how much data was left in percent in each channel
    artifactIndicesVid(1,x) = index2;
end


% Find all channels with more than 75% data left. Remove all channel 
% combinations where this is not the case
pos1 = abs(artifactIndicesMob(1,:)) >= 0.7;
pos2 = abs(artifactIndicesVid(1,:)) >= 0.7;

pairedChannelsMob = pairedChannelsMob(1,pos);
pairedChannelsVid = pairedChannelsVid(1,pos);


% Pre-define table entries (empty tables, if an all channel combinations too much data was removed)
FirstChM = {}; SecondChM = {};
FirstChV = {}; SecondChV = {};
allCorr = []; Lags = []; DataLeft = []; GarbageInChannel = zeros(4,0); bestLag = [];

if (size(pairedChannels,2) > 1)

% Run all channel combinations and see which lag gives the best correlation
% allLags = zeros(1,nchoosek(size(pairedChannelsNum,2),2));
allLags = [];
n = 1;
for x = 1:size(pairedChannelsNum,2)
    for y = (x+1):size(pairedChannelsNum,2)
        
        minLength = min(size(fullSignal1,2),size(fullSignal2,2));
        bipolar1 = fullSignal1(pairedChannelsNum(1,x),1:minLength) - fullSignal1(pairedChannelsNum(1,y),1:minLength);
        bipolar2 = fullSignal2(pairedChannelsNum(2,x),1:minLength) - fullSignal2(pairedChannelsNum(2,y),1:minLength);
        
        % l=0, computes lag automatically
        lag = findLag(lagStored, patNr, bipolar1, bipolar2);
        allLags(n) = lag;
        GarbageInChannel(:,n) = [artifactIndices(1,x); artifactIndices(1,y); artifactIndices(2,x); artifactIndices(2,y)];
        n = n + 1;
    end
end


% Set most frequent lag to new lag
bestLag = mode(allLags);

%bestLag
n=1;
for x= 1:size(pairedChannelsNum,2)
    for y= (x+1):size(pairedChannelsNum,2)
        if abs(allLags(n) - bestLag)<5
            lag = allLags(n);
        else
            lag = bestLag;
        end
        minLength = min(size(fullSignal1,2),size(fullSignal2,2));
        bipolar1 = fullSignal1(pairedChannelsNum(1,x),1:minLength) - fullSignal1(pairedChannelsNum(1,y),1:minLength);
        bipolar2 = fullSignal2(pairedChannelsNum(2,x),1:minLength) - fullSignal2(pairedChannelsNum(2,y),1:minLength);
        [index, bipolar1, bipolar2] = shiftData(lag, bipolar1, bipolar2);
        
        channelCorr = corr(bipolar1', bipolar2','Type','Pearson');
        
        FirstChM(n) = pairedChannels(1,x); 
        SecondChM(n) = pairedChannels(1,y);
        FirstChV(n) = pairedChannels(2,x);
        SecondChV(n) = pairedChannels(2,y);
        allCorr(n) = channelCorr;
        Lags(n) = lag;
        DataLeft(n) = index; %size(toCompare1,2)/size(d1,2);
        n = n+1;
    end
end

end

%
%Put results in a table

tbFull=table(FirstChM',SecondChM',FirstChV',SecondChV',allCorr',Lags', DataLeft',GarbageInChannel(1,:)',GarbageInChannel(2,:)',GarbageInChannel(3,:)',GarbageInChannel(4,:)','VariableNames',{'Ch1M' 'Ch2M'  'Ch1V' 'Ch2V' 'Correlation' 'Delay' 'Percent' 'PercentInChannel1' 'PercentInChannel2' 'PercentInChannel3' 'PercentInChannel4'});

pairedChannelsNew = pairedChannels;

%Compute mean correlation
meanCor = nanmean(tbFull.Correlation);


end