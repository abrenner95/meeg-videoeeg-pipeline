function [lag] = findLag(lagStored, patNr, data1, data2)

% Find the lag between the two
% Two options:
% lagStored == 1 - load previously computed lag
% lagStored == 0 - (re-)compute lag
if lagStored == 0
    isOK = isfinite(data1) & isfinite(data2);
    lag = finddelay(data1(isOK), data2(isOK));
else
    fpath = strcat(pwd(), '/storage/lags/', patNr, '.mat');
    load(fpath, 'lag');
end

end

