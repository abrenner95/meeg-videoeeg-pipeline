%Normalization through division by percentile pr
%%Very first data preparation
%filtering, resampling, single to double precision, normalization

function [data1, data2] = normalizeEEG(data1, data2, p)

%Normalization through division by percentile pr
data11 = reshape(data1,[1,size(data1,1)*size(data1,2)]);
data1 = data1./prctile(abs(data11),p,2);

data22 = reshape(data2,[1,size(data2,1)*size(data2,2)]);
data2 = data2./prctile(abs(data22),p,2);

end