patients = [1,2,3,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21];

shuffled = patients(randperm(length(patients)));

trainSet = shuffled(1:14);
testSet = shuffled(15:end);

save(strcat(pwd, '/trainSet.mat'), 'trainSet');
save(strcat(pwd, '/testSet.mat'), 'testSet');